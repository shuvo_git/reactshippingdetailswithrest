import React, { useState } from 'react';

import SortableTbl from 'react-sort-search-table';
import './App.css';
import { Card,CardBody,CardText, CardTitle, Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,FormGroup,Label,Input,FormText,Badge,Alert } from 'reactstrap';
import DataTable, { createTheme } from 'react-data-table-component';
import '@empd/reactable/lib/styles.css';
import axios from 'axios';
import data from './db.json';

require.context('./fonts', true, /\.?/);


    
        
class ShippingTable extends React.Component{
    constructor(props)
    {
        super(props);
        this.state = {
            datas : [],
            col : [
                "id",
                "name",
                "mode",
                "type",
                "destination",
                "origin",
                "total",
                "status",
                "userId",
                "view",
                "update"
            ],
            tHead : [
                "id",
                "name",
                "mode",
                "type",
                "destination",
                "origin",
                "total",
                "status",
                "userid",
                "view",
                "update"
                
            ]
        }
        
    }
    
    componentDidMount()
    {
        axios.get('http://localhost:3000/shipments').then((Response)=>{
            this.setState(
                {
                    datas:Response.data,
                    
                }
            )
            console.log(this.state.datas);
        });
    }

    componentWillUnmount() {
        this.unsubscribe();
      }
    

    render() 
	{
        console.log(this.state.datas);
        
		return (
            <SortableTbl tblData={this.state.datas}
                customTd={[
                
                    {custd: BaseProductViewComponent, keyItem: "view"},
                    {custd: BaseProductEditComponent, keyItem: "update"}
                ]}
                tHead={this.state.tHead}
                dKey={this.state.col}
                search={true}
                defaultCSS={true}
                defaultRowsPerPage= {20}
            />
			
		);
	}
}     




class BaseProductViewComponent extends React.Component{
	constructor(props) {
		super(props);
        //this.deleteItem = this.deleteItem.bind(this);
        
        this.state = {
            EditModal : false,
        };
    }
    
    toggleEditModal()
    {
        this.setState({
            EditModal: !this.state.EditModal,
        });
    }
	
	render () {
        
		return (	
			<td >	
				<Button color='success' size='sm' className='mr-2' onClick={this.toggleEditModal.bind(this)} >View</Button>
                <Modal isOpen={this.state.EditModal} toggle={this.toggleEditModal.bind(this)} fade={false}>
                    <ModalHeader toggle={this.toggleEditModal.bind(this)}>
                    <h3> {this.props.rowData.name} </h3>
                    </ModalHeader>
                    <ModalBody>
                        
                        <Form>
                            <FormGroup>
                                <Label for="ID">ID : </Label>
                                <Badge color="success" pill>{" "+this.props.rowData.id+" "}</Badge>
                                </FormGroup>
                            <DataTable
                                title="Cargo"
                                theme="dark"
                                columns={[
                                    {
                                      name: 'Type',
                                      selector: 'type',
                                      sortable: true,
                                    },
                                    {
                                      name: 'description',
                                      selector: 'description',
                                      sortable: true,
                                      right: true,
                                    },
                                    {
                                      name: 'volume',
                                      selector: 'volume',
                                      sortable: true,
                                      right: true,
                                    },
                                  ]}
                                data={this.props.rowData.cargo}
                            />
                            <FormGroup>
                                <Label for="mode">Mode : </Label>
                                {" "+this.props.rowData.mode+" "}
                                <Label for="type">Type : </Label>
                                {" "+this.props.rowData.type}
                            </FormGroup>
                            <FormGroup>
                                <Label for="destination">Destination : </Label>
                                {" "+this.props.rowData.destination+"     "}

                                <Label for="origin">Origin : </Label>
                                {" "+this.props.rowData.origin}
                            </FormGroup>
                            <DataTable
                                title="Services"
                                theme="dark"
                                columns={[
                                    {
                                      name: 'Type',
                                      selector: 'type',
                                      sortable: true,
                                    },
                                    
                                    {
                                      name: 'value',
                                      selector: 'value',
                                      sortable: true,
                                      right: true,
                                    },
                                  ]}
                                data={this.props.rowData.services}
                            />
                            <FormGroup>
                                <Card sm="12">
                                    <CardBody>
                                        <CardTitle>Total</CardTitle>
                                        <CardText>
                                        <h4>{" "+this.props.rowData.total}</h4>
                                        </CardText>
                                    </CardBody>
                                </Card>
                                <Card sm="12">
                                    <CardBody>
                                        <CardTitle>Status</CardTitle>
                                        <CardText>
                                        <h4>{" "+this.props.rowData.status}</h4>
                                        </CardText>
                                    </CardBody>
                                </Card>
                                <Card sm="12">
                                    <CardBody>
                                        <CardTitle>User ID</CardTitle>
                                        <CardText>
                                        <h4>{" "+this.props.rowData.userId}</h4>
                                        </CardText>
                                    </CardBody>
                                </Card>
                            </FormGroup>
                            
                            
                            
                            
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                    <Button color="primary" onClick={this.toggleEditModal.bind(this)}>Ok</Button>{' '}
                    </ModalFooter>
                </Modal>
            </td>
		);
	}
}








class BaseProductEditComponent extends React.Component
{
	constructor(props) {
		super(props);

        this.state = {
            EditModal : false,
            currName : this.props.rowData.name
        };
        
	}
	editItem(){
		
        let id = this.props.rowData.id;
        
        try {
            const response = axios.patch('http://localhost:3000/shipments/'+id, 
            { "name":  this.state.currName});
            console.log('Returned data:', response);
            this.toggleEditModal();
          } catch (e) {
            console.log(`Axios request failed: ${e}`);
          }

    }
    
    toggleEditModal()
    {
        this.setState({
            EditModal: !this.state.EditModal,
        });
    }

    
	render () {
        
        return (	
			<td >	
				<Button color='primary' size='sm' className='mr-2' onClick={this.toggleEditModal.bind(this)} >Update</Button>
                <Modal isOpen={this.state.EditModal} toggle={this.toggleEditModal.bind(this)} fade={false}>
                    <ModalHeader toggle={this.toggleEditModal.bind(this)}>
                        Edit Name
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label for="name">Name</Label>
                            <Input type="text" name="name" id="idName" value={this.state.currName} placeholder="Fill the name" 
                            onChange={(e)=>{
                                this.setState({currName:e.target.value});
                            }} />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                    <Button color="primary" onClick={this.editItem.bind(this)}>Update</Button>{' '}
                    <Button color="secondary"  onClick={this.toggleEditModal.bind(this)}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </td>

		);
	}
}

    
    

/*
class Parent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };
        this.toggle = this.toggle.bind(this);
    };

    toggle() {
        this.setState({modal: !this.state.modal});
    }

    render() {
        return (
            <div>
                <button onClick={ this.toggle}>Contact Us</button>
                <Modal isOpen={this.state.modal}  fade={false}
                       toggle={this.toggle} >
                    <ModalHeader toggle={this.toggle}>
                        Modal title
                    </ModalHeader>
                    <ModalBody>
                        Lorem ipsum dolor sit amet,
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.toggle}>
                            Do Something
                        </Button>{' '}
                        <Button onClick={this.toggle}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
//const data2 = [{ id: 1, title: 'Conan the Barbarian', year: '1982' } ];
const columns = [
  {
    name: 'Type',
    selector: 'type',
    sortable: true,
  },
  {
    name: 'description',
    selector: 'description',
    sortable: true,
    right: true,
  },
  {
    name: 'volume',
    selector: 'volume',
    sortable: true,
    right: true,
  },
];

class MyComponent extends React.Component {
  render() {
    return (
      <DataTable
        title="Arnold Movies"
        columns={columns}
        data={data.shipments[0].cargo}
      />
    )
  }
};
*/


function App() {
  return (
    <div className="App">
        <ShippingTable/>
    </div>
  );
}



 
 

export default App;
